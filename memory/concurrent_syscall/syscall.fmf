summary: Perform concurrency testing on system calls
description: |
    Perform concurrency testing on system calls.
    Confirm nothing failed, broke or warnings after execution.
    Test Inputs:
        https://gitlab.com/redhat/centos-stream/tests/ltp/-/raw/${LTP_VERSION}/runtest/syscalls
    Expected results:
        [   PASS   ] :: Command './runtest.sh' (Expected 0, got 0)
        [   PASS   ] :: File '/var/tmp/tmt/run-###/memory/concurrent_syscall/plan/data/concurrent_syscalls.log' should contain 'failed.* 0$'
        [   PASS   ] :: File '/var/tmp/tmt/run-###/memory/concurrent_syscall/plan/data/concurrent_syscalls.log' should contain 'broken.* 0$'
        [   PASS   ] :: File '/var/tmp/tmt/run-###/memory/concurrent_syscall/plan/data/concurrent_syscalls.log' should contain 'warnings.* 0$'
    Results location:
        output.txt
contact: Stephen Bertram <sbertram@redhat.com>
test: bash ./runtest.sh
id: 19ccd078-d9ca-4502-8ea0-b951b733e054
framework: beakerlib
require+:
  - procmail
  - automake
  - autoconf
  - flex
  - bison
  - rng-tools
  - rsyslog
  - util-linux
  - bc
  - gcc
  - bzip2
  - rpm-build
  - libaio-devel
  - libcap-devel
  - numactl-devel
  - numactl
  - strace
  - wget
  - python3
  - ntpsec
  - type: file
    pattern:
      - /distribution/ltp/generic
      - /cki_lib
      - /distribution/ltp/include
      - /distribution/ltp/include-ng
      - /distribution/ltp/kirk
recommend+:
  - python3-click
duration: 180m
environment+:
    TESTARGS: SYSCALLS
    LTP_TIMEOUT_MUL: 20
    SKIPTESTS: "getxattr04"
    OPTS: "-w 2 -p"
