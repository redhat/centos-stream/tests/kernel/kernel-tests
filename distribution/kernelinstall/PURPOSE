This task does following:
1. installs new kernel
1a. via yum if possible
1b. directly from brew via http, this includes brewroot and kernelarchive

If any of above fails, whole recipe gets aborted.
2. sets new kernel as default
   either directly by editing boot config or on new distros by using grubby
3. makes reboot
4. submit logs, including output from lspci, lsmod, ip link show, ethtool, etc.

You must pass three testarg environment variables:

KERNELARGNAME
KERNELARGVERSION
KERNELARGVARIANT

KERNELARGNAME is name of package, for example: 'kernel', 'kernel-aarch64', 'kernel-pegas'
KERNELARGVERSION is the version-release
KERNELARGVARIANT is variant: up, xen, debug
"up" in newer distros will be equivalent to SMP.

You CAN optionally pass KERNELARGTMPREPO and KERNELARGPERMREPO environment
variables:

KERNELARGTMPREPO="http://download.devel.redhat.com/rel-eng/latest-RHEL-7/compose/Server/x86_64/os"
KERNELARGTMPREPO="baseurl1 baseurl2"

This variable contains a SPACE SEPARATED list of REPO URLS. This task will
create a TEMPORARY repository just for the purpose of this task.  The
repository will be disabled at the end of the task.

KERNELARGPERMREPO="baseurl1 baseurl2"

This variable contains a SPACE SEPARATED list of REPO URLS. In contrast with
repositories defined with KERNELARGTMPREPO this will create a PERMANENT
repository which will stay enabled even after kernel install task will finish.

You can optionally pass KERNELARGEXTRAMODULES environment variable:
KERNELARGEXTRAMODULES="1"

If this variable is set to 1 then the kernel-modules-extra will be installed
along with kernel-modules rpm. On RHEL8 and later distributions the extra
package is not installed by default. Same for KERNELARGINTERNALMODULES="1",
kernel-modules-internal will be installed if set to 1.

Sample Beaker job XML:

<task name="/distribution/kernelinstall" role="STANDALONE">
    <params>
        <param name="KERNELARGNAME" value="kernel"/>
        <param name="KERNELARGVERSION" value="2.6.18-238.50.1.el5"/>
        <param name="KERNELARGVARIANT" value="up"/>
    </params>
</task>

bkr workflow-tomorrow invocation of /distribution/kernelinstall:

bkr workflow-tomorrow --whiteboard "verify migrate_pages [Bug 848473]" \
    --distro "RHEL5.10-Server-20130612.0.n" \
    --arch=x86_64 \
    --task=/distribution/kernelinstall \
    --taskparam="KERNELARGVERSION=2.6.18-361.el5" \
    --taskparam="KERNELARGNAME=kernel" \
    --taskparam="KERNELARGVARIANT=up" \
    --task="/kernel/distribution/ltp/generic" \
    --machine="$beaker_machine_hostname"
