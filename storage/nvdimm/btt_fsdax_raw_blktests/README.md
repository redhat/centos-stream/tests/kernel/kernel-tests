# storage/nvdimm/btt_fsdax_raw_blktests

Storage: nvdimm blktests with btt fsdax raw devices

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
