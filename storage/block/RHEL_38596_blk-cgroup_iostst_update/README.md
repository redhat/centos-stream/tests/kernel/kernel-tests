# storage/block/RHEL_38596_blk-cgroup_iostst_update

Storage: blk-cgroup properly propagate the iostat update up the hierarchy

## How to run it

Please refer to the top-level README.md for common dependencies.

### Install dependencies

```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test

```bash
bash ./runtest.sh
```
