# storage/scsi/RHEL_35659_close_removed_sg_device

Storage: closing sg device that is removed

## How to run it

Please refer to the top-level README.md for common dependencies.

### Install dependencies

```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test

```bash
bash ./runtest.sh
```
