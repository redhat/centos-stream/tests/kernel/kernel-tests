# kfdtest test suite for testing of the AMD in-tree driver
AMD kfdtest suite for testing the AMD in-tree driver

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
